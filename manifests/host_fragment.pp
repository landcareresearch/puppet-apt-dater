# @summary This define is used to export/collect the required connection information
# from hosts to managers.
#
# @param host_ip_address
#   The ip address to use for updating the host entry.
#   Default: $::ip_address
#
# @param update_hosts
#   Set true if this defined type should update the hosts entry
#   based on the ssh_name and host_ip_address,
#   Default: false,
#
define apt_dater::host_fragment (
  $customer,
  $ssh_user,
  $ssh_name,
  $ssh_port,
  $host_ip_address,
  $update_hosts    = false,
) {
  validate_string($customer)
  validate_string($ssh_user)
  validate_string($ssh_name)
  validate_string($ssh_port)

  include ::apt_dater

  file { "${apt_dater::manager_fragments_dir}/${customer}:${ssh_user}\
@${ssh_name}:${ssh_port}":
    ensure  => present,
    content => '',
  }

  if $update_hosts {
    # add the host ip address
    host { $ssh_name:
      ip => $host_ip_address,
    }
  }
}
