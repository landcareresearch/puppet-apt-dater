# Puppet module: apt_dater

[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetAptDater_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetAptDater_Build&guest=1)

## About

Puppet module to manage apt_dater.

## History

The following is in chronological order:

* Code extracted from example42/puppet-apt, check it if needed a full apt ecosystem management module.
* Originally written by Boian Mihailov - boian.mihailov@gmail.com
* Added features by Marco Bonetti
* Adapted to Example42 NextGen layout by Alessandro Franceschi
* Features removed by Baptiste Grenier.
* Yum-based update manager taken from https://github.com/DE-IBH/apt-dater-host/
* Project forked from https://github.com/gnubila-france/puppet-apt_dater.
* Extended to work with the new XML configuration format by landcareresearch.

## Description

This module installs and manages [apt-dater](http://www.ibh.de/apt-dater/)
to manage centrally controlled updates via ssh on deb-based and yum-based
systems.

All the variables used in this module are defined in the apt_dater::params class
(File: $MODULEPATH/apt_dater/manifests/params.pp).

## Examples

### Client

Configure a host to be controlled by apt-dater

```puppet
  class { 'apt_dater':
    customer     => 'ACME Corp.',
    ssh_key_type => 'ssh-rsa',
    ssh_key      => template('site/apt-dater.pub.key');
  }
```

### Manager

Configure an apt-dater controller (no self-management) for root

```puppet
  class { 'apt_dater':
    role            => 'manager',
    manager_ssh_key => template('site/apt-dater.priv.key');
  }
```

## API

See REFERENCES.md
