# apt_dater Puppet Module Changelog

## 2021-02-23 Release 6.0.1

- Updated the documentation.
- Added a dependency.

## 2021-02-23 Release 6.0.0

- Converted to latest PDK.
