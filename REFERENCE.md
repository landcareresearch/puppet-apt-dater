# Reference
<!-- DO NOT EDIT: This document was generated by Puppet Strings -->

## Table of Contents

**Classes**

* [`apt_dater`](#apt_dater): This is the main apt_dater class to configure a node for being managed by apt-dater.
* [`apt_dater::host`](#apt_daterhost): Adding a host to apt dater.
* [`apt_dater::manager`](#apt_datermanager): Manages apt dater.
* [`apt_dater::params`](#apt_daterparams): This class defines default parameters used by the main module class apt_dater

**Defined types**

* [`apt_dater::host_fragment`](#apt_daterhost_fragment): This define is used to export/collect the required connection information

## Classes

### apt_dater

This is the main apt_dater class to configure a node for being managed by apt-dater.

#### Parameters

The following parameters are available in the `apt_dater` class.

##### `my_class`

Name of a custom class to autoload to manage module's customizations
If defined, apt_dater class will automatically "include $my_class"
Can be defined also by the (top scope) variable $apt_dater_myclass

##### `version`

Data type: `Any`

The package version, used in the ensure parameter of package type.
Default: present. Can be 'latest' or a specific version number.
Note that if the argument absent (see below) is set to true, the
package is removed, whatever the value of version parameter.

Default value: $apt_dater::params::version

##### `absent`

Set to 'true' to remove package(s) installed by module
Can be defined also by the (top scope) variable $apt_dater_absent

##### `noops`

Set noop metaparameter to true for all the resources managed by the module.
Basically you can run a dryrun for this specific module if you set
this to true. Default: undef

##### `debug`

Set to 'true' to enable modules debugging
Can be defined also by the (top scope) variables $apt_dater_debug and $debug

##### `role`

Data type: `Any`

One of 'host', 'manager' or 'all'.

Default value: $apt_dater::params::role

##### `customer`

Data type: `Any`

A grouping to be displayed in the apt-dater interface. Should be a simple alphanumeric string.

Default value: $apt_dater::params::customer

##### `package`

Data type: `Any`

The name of apt-dater package

Default value: $apt_dater::params::package

##### `host_package`

Data type: `Any`

The name of apt-dater-host package

Default value: $apt_dater::params::host_package

##### `user`

Which user to use when connecting to the hosts. By default the user is called
"apt-dater" and created for you.

##### `home_dir`

Where to put the config and ssh keys for the apt_dater::user.

##### `reuse_user`

If your user is managed elsewhere, set this to true. Then this class doesn't touch
the user.
This class also will not manage sudo for the user either.
If user is set, this class will create and manage the user and also
create the permissions in sudoers.

##### `reuse_ssh`

Data type: `Any`

If your ssh connection is managed elsewhere, set this to true. Then this class
doesn't touch the ssh keys.

Default value: $apt_dater::params::reuse_ssh

##### `ssh_key_options`

Data type: `Any`

The options for the ssh key, as required by ssh_authorized_key.

Default value: $apt_dater::params::ssh_key_options

##### `ssh_key_type`

Data type: `Any`

The type for the ssh key, as required by ssh_authorized_key.

Default value: $apt_dater::params::ssh_key_type

##### `ssh_key`

Data type: `Any`

The ssh key, as required by ssh_authorized_key.

Default value: $apt_dater::params::ssh_key

##### `manager_user`

Data type: `Any`

The user managing apt-dater. Only used on hosts with the 'manager' role.

Default value: $apt_dater::params::manager_user

##### `manager_ssh_dir`

Where to put the secret apt-dater identity. Only used on hosts with the 'manager' role.

##### `manager_ssh_key`

Data type: `Any`

The secret apt-dater identity. Only used on hosts with the 'manager' role.

Default value: $apt_dater::params::manager_ssh_key

##### `host_ip_address`

Data type: `Any`

The ip address for the host.

Default value: $::ipaddress

##### `host_update_cmd`

Data type: `Any`



Default value: $apt_dater::params::host_update_cmd

##### `host_user`

Data type: `Any`



Default value: $apt_dater::params::host_user

##### `host_home_dir`

Data type: `Any`



Default value: $apt_dater::params::host_home_dir

##### `reuse_host_user`

Data type: `Any`



Default value: $apt_dater::params::reuse_host_user

##### `ssh_port`

Data type: `Any`



Default value: $apt_dater::params::ssh_port

##### `manager_home_dir`

Data type: `Any`



Default value: $apt_dater::params::manager_home_dir

##### `update_hosts`

Data type: `Any`



Default value: `false`

### apt_dater::host

Adding a host to apt dater.

### apt_dater::manager

Manages apt dater.

### apt_dater::params

Operating Systems differences in names and paths are addressed here

## Defined types

### apt_dater::host_fragment

from hosts to managers.

#### Parameters

The following parameters are available in the `apt_dater::host_fragment` defined type.

##### `host_ip_address`

Data type: `Any`

The ip address to use for updating the host entry.
Default: $::ip_address

##### `update_hosts`

Data type: `Any`

Set true if this defined type should update the hosts entry
based on the ssh_name and host_ip_address,
Default: false,

Default value: `false`

##### `customer`

Data type: `Any`



##### `ssh_user`

Data type: `Any`



##### `ssh_name`

Data type: `Any`



##### `ssh_port`

Data type: `Any`



